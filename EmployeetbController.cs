using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using GreetingService.Models;

namespace GreetingService.Controllers
{
public class EmployeetbsController : ApiController
{
private EmployeeDBEntities db = new EmployeeDBEntities();

    // GET: api/Employeetbs
    public IQueryable<Employeetb> GetEmployeetbs()
    {
        return db.Employeetbs;
    }

    // GET: api/Employeetbs/5
    [ResponseType(typeof(Employeetb))]
    public IHttpActionResult GetEmployeetb(string id)
    {
        Employeetb employeetb = db.Employeetbs.Find(id);
        if (employeetb == null)
        {
            return NotFound();
        }

        return Ok(employeetb);
    }

    // PUT: api/Employeetbs/5
    [ResponseType(typeof(void))]
    public IHttpActionResult PutEmployeetb(string id, Employeetb employeetb)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        if (id != employeetb.id)
        {
            return BadRequest();
        }

        db.Entry(employeetb).State = EntityState.Modified;

        try
        {
            db.SaveChanges();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!EmployeetbExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return StatusCode(HttpStatusCode.NoContent);
    }

    // POST: api/Employeetbs
    [ResponseType(typeof(Employeetb))]
    public IHttpActionResult PostEmployeetb(Employeetb employeetb)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        db.Employeetbs.Add(employeetb);

        try
        {
            db.SaveChanges();
        }
        catch (DbUpdateException)
        {
            if (EmployeetbExists(employeetb.id))
            {
                return Conflict();
            }
            else
            {
                throw;
            }
        }

        return CreatedAtRoute("DefaultApi", new { id = employeetb.id }, employeetb);
    }

    // DELETE: api/Employeetbs/5
    [ResponseType(typeof(Employeetb))]
    public IHttpActionResult DeleteEmployeetb(string id)
    {
        Employeetb employeetb = db.Employeetbs.Find(id);
        if (employeetb == null)
        {
            return NotFound();
        }

        db.Employeetbs.Remove(employeetb);
        db.SaveChanges();

        return Ok(employeetb);
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            db.Dispose();
        }
        base.Dispose(disposing);
    }

    private bool EmployeetbExists(string id)
    {
        return db.Employeetbs.Count(e => e.id == id) > 0;
    }
}
}