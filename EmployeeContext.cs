//------------------------------------------------------------------------------
//
// This code was generated from a template.
//
// Manual changes to this file may cause unexpected behavior in your application.
// Manual changes to this file will be overwritten if the code is regenerated.
//
//------------------------------------------------------------------------------

namespace GreetingService.Models
{
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

public partial class EmployeeDBEntities : DbContext
{
    public EmployeeDBEntities()
        : base("name=EmployeeDBEntities")
    {
    }

    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    {
        throw new UnintentionalCodeFirstException();
    }

    public virtual DbSet<Employeetb> Employeetbs { get; set; }
    public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
    public object Employees { get; internal set; }
}
}